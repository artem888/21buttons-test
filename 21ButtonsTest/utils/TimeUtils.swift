//
//  TimeUtils.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 29.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import Foundation

struct TimeUtils {
    static func formattedTime(with seconds: Int) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .full
        
        return formatter.string(from: TimeInterval(seconds))!
    }
}
