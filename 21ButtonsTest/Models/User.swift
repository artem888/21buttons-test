//
//  User.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 29.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import Foundation
import Unbox

struct User {
    let name: String
}

extension User: Unboxable {
    init(unboxer: Unboxer) throws {
        self.name = try unboxer.unbox(keyPath: "names.international")
    }
}
