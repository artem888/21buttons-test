//
//  Run.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 29.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import Foundation
import Unbox

struct Run {
    let videoUrl: URL?
    let runTimeInSeconds: Int
    
    // player can be either guest or user
    let guestName: String?
    let userId: String?
}

extension Run: Unboxable {
    init(unboxer: Unboxer) throws {
        self.videoUrl = unboxer.unbox(keyPath: "videos.links.0.uri")
        self.runTimeInSeconds = unboxer.unbox(keyPath: "times.primary_t") ?? 0
        self.guestName = unboxer.unbox(keyPath: "players.0.name")
        self.userId = unboxer.unbox(keyPath: "players.0.id")
    }
}
