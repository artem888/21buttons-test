//
//  Game.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 27.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import Foundation
import Unbox

struct Game {
    let id: String
    let name: String
    let logoUrl: URL?
}

extension Game: Unboxable {
    init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "id")
        self.name = try unboxer.unbox(keyPath: "names.international")
        self.logoUrl = unboxer.unbox(keyPath: "assets.cover-medium.uri")
    }
}
