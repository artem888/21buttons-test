//
//  URLExtensions.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 29.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import Foundation

extension URL {
    func appendindQueryItems(_ items: [URLQueryItem]) -> URL {
        guard var urlComponents = URLComponents(string: absoluteString) else { return self }
        
        urlComponents.queryItems = items
        
        return urlComponents.url ?? self
    }
}
