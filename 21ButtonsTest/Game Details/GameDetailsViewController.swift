//
//  GameDetailsViewController.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 29.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import UIKit
import Nuke
import RxCocoa
import RxSwift

final class GameDetailsViewController: UIViewController {
    private var gameDetailsView: GameDetailsView!
    private let viewModel: GameDetailsViewModelProtocol
    
    private let bag = DisposeBag()
    
    init(viewModel: GameDetailsViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.gameDetailsView = GameDetailsView()
        self.gameDetailsView.delegate = self
        self.view = self.gameDetailsView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gameDetailsView.render()
        
        // fill in some static data
        gameDetailsView.gameNameLabel.text = "Game name: \(viewModel.gameName)"
        if let url = viewModel.gameLogoURL {
            Nuke.loadImage(
                with: url,
                options: ImageLoadingOptions(
                    placeholder: UIImage(named: "placeholder"),
                    transition: .fadeIn(duration: 0.33)
                ),
                into: gameDetailsView.logoImageView
            )
        }
        
        rxBind()
        
        // perform api request
        viewModel.fetchRunAndUser()
    }
    
    // MARK: Private
    private func rxBind() {
        viewModel.time
            .drive(gameDetailsView.timeLabel.rx.text)
            .disposed(by: bag)
        viewModel.playVideoButtonHidden
            .drive(gameDetailsView.playVideoButton.rx.isHidden)
            .disposed(by: bag)
        viewModel.userName
            .drive(gameDetailsView.userNameLabel.rx.text)
            .disposed(by: bag)
    }
}

extension GameDetailsViewController: GameDetailsViewDelegate {
    func gamesDetailsViewPlayButtonDidTap(_ view: GameDetailsView) {
        if let url = viewModel.videoURL() {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
    }
}
