//
//  GameDetailsViewModel.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 29.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol GameDetailsViewModelProtocol {
    var api: SpeedrunAPIProtocol.Type { get }
    
    var gameName: String { get }
    var gameLogoURL: URL? { get }
    var time: Driver<String> { get }
    var userName: Driver<String> { get }
    var playVideoButtonHidden: Driver<Bool> { get }
    
    func fetchRunAndUser()
    func videoURL() -> URL?
}

final class GameDetailsViewModel: GameDetailsViewModelProtocol {
    private enum Constants {
        static let timeFetchError = "###Error### Failed to fetch time of run"
        static let userNameFetchError = "###Error### Failed to fetch user name"
    }
    
    var userName: Driver<String>
    var playVideoButtonHidden: Driver<Bool>
    var time: Driver<String>
    var gameLogoURL: URL?
    var gameName: String
    var api: SpeedrunAPIProtocol.Type
    
    private let gameId: String
    private var runTimeRelay = BehaviorRelay<String>(value: "")
    private var videoUrlRelay = BehaviorRelay<URL?>(value: nil)
    private var userNameRelay = BehaviorRelay<String>(value: "")
    
    private let bag = DisposeBag()
    
    init(api: SpeedrunAPIProtocol.Type = SpeedrunAPI.self,
         gameName: String,
         gameLogoURL: URL?,
         gameId: String) {
        self.api = api
        self.gameName = gameName
        self.gameLogoURL = gameLogoURL
        self.gameId = gameId
        self.time = Driver.empty()
        self.playVideoButtonHidden = Driver.empty()
        self.userName = Driver.empty()
        rxBind()
    }
    
    func fetchRunAndUser() {
        let sharedRunRequest = api.run(with: gameId).share()
        
        sharedRunRequest.subscribe(onNext: { [weak self] run in
            guard let self = self else { return }
            self.runTimeRelay.accept("Time of run: \(TimeUtils.formattedTime(with: run.runTimeInSeconds))")
            self.videoUrlRelay.accept(run.videoUrl)
            if let guestName = run.guestName {
                self.userNameRelay.accept("Guest user name: \(guestName)")
            }
        }, onError: { [weak self] _ in
            guard let self = self else { return }
            self.runTimeRelay.accept(Constants.timeFetchError)
            self.userNameRelay.accept(Constants.userNameFetchError)
            self.videoUrlRelay.accept(nil)
        }).disposed(by: bag)
        
        sharedRunRequest
            .filter { run in
                return run.userId != nil
            }.flatMapLatest { run in
                return self.api.user(with: run.userId!)
            }.subscribe(onNext: { [weak self] user in
                guard let self = self else { return }
                self.userNameRelay.accept("User name: \(user.name)")
            }, onError: { [weak self] _ in
                guard let self = self else { return }
                self.userNameRelay.accept(Constants.userNameFetchError)
            })
            .disposed(by: bag)
    }
    
    func videoURL() -> URL? {
        return videoUrlRelay.value
    }
    
    // MARK: Private
    private func rxBind() {
        time = runTimeRelay.asDriver(onErrorJustReturn: Constants.timeFetchError)
        userName = userNameRelay.asDriver(onErrorJustReturn: Constants.userNameFetchError)
        playVideoButtonHidden = videoUrlRelay
            .map { return $0 == nil }
            .asDriver(onErrorJustReturn: true)
    }
}
