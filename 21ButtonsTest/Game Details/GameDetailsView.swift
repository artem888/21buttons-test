//
//  GameDetailsView.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 29.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import UIKit

protocol GameDetailsViewDelegate: class {
    func gamesDetailsViewPlayButtonDidTap(_ view: GameDetailsView)
}

final class GameDetailsView: UIView {
    private enum ViewConstants {
        static let defaultSpacing: CGFloat = 16.0
    }
    
    weak var delegate: GameDetailsViewDelegate?
    
    private var stackView: UIStackView!
    private(set) var logoImageView: UIImageView!
    private(set) var gameNameLabel: UILabel!
    private(set) var timeLabel: UILabel!
    private(set) var userNameLabel: UILabel!
    private(set) var playVideoButton: UIButton!
    
    func render() {
        self.backgroundColor = .white
        self.stackView = createStackView()
    }
    
    @objc func playTapped() {
        delegate?.gamesDetailsViewPlayButtonDidTap(self)
    }
    
    // MARK: Private
    private func createStackView() -> UIStackView {
        self.logoImageView = createLogoImageView()
        self.gameNameLabel = createGameNameLabel()
        self.timeLabel = createTimeLabel()
        self.userNameLabel = createUserNameLabel()
        self.playVideoButton = createPlayVideoButton()
        
        let sv = UIStackView(arrangedSubviews: [
            logoImageView,
            gameNameLabel,
            timeLabel,
            userNameLabel,
            playVideoButton
        ])
        sv.axis = .vertical
        sv.alignment = .leading
        sv.spacing = ViewConstants.defaultSpacing
        sv.translatesAutoresizingMaskIntoConstraints = false
        addSubview(sv)
        NSLayoutConstraint.activate([
            sv.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor,
                                    constant: ViewConstants.defaultSpacing),
            sv.leadingAnchor.constraint(equalTo: self.leadingAnchor,
                                        constant: ViewConstants.defaultSpacing)
        ])
        
        return sv
    }
    
    private func createGameNameLabel() -> UILabel {
        let l = UILabel()
        return l
    }
    
    private func createLogoImageView() -> UIImageView {
        let iv = UIImageView()
        iv.contentMode = .center
        iv.clipsToBounds = true
        
        return iv
    }
    
    private func createTimeLabel() -> UILabel {
        let l = UILabel()
        return l
    }
    
    private func createUserNameLabel() -> UILabel {
        let l = UILabel()
        return l
    }
    
    private func createPlayVideoButton() -> UIButton {
        let btn = UIButton(type: .system)
        btn.setTitle("Play run video", for: .normal)
        btn.addTarget(self,
                      action: #selector(playTapped),
                      for: .touchUpInside)
        
        return btn
    }
}
