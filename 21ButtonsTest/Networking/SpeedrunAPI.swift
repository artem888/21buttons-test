//
//  SpeedrunAPI.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 27.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire
import Unbox

protocol SpeedrunAPIProtocol {
    static func games() -> Observable<[Game]>
    static func run(with gameId: String) -> Observable<Run>
    static func user(with userId: String) -> Observable<User>
}

struct SpeedrunAPI: SpeedrunAPIProtocol {
    private enum Address: String {
        case games = "games"
        case runs = "runs"
        case users = "users"
        
        private var baseURL: String { return "https://www.speedrun.com/api/v1/" }
        
        var url: URL {
            return URL(string: baseURL.appending(rawValue))!
        }
    }
    
    enum ApiError: Error {
        case requestFailed
        case responseMappingFailed
    }
    
    static func games() -> Observable<[Game]> {
        return requestJSON(.get, Address.games.url)
            .observeOn(SerialDispatchQueueScheduler(qos: .background))
            .map { _, jsonResponse in
                guard let jsonResponse = jsonResponse as? [String: AnyObject],
                let data = jsonResponse["data"] as? [[String: AnyObject]] else {
                    throw ApiError.responseMappingFailed
                }
                
                do {
                    let games: [Game] = try unbox(dictionaries: data)
                    return games
                } catch {
                    throw ApiError.responseMappingFailed
                }
        }
    }
    
    static func run(with gameId: String) -> Observable<Run> {
        let queryItem = URLQueryItem(name: "game",
                                     value: gameId)
        
        return requestJSON(.get, Address.runs.url.appendindQueryItems([queryItem]))
            .observeOn(SerialDispatchQueueScheduler(qos: .background))
            .map { _, jsonResponse in
                guard let jsonResponse = jsonResponse as? [String: AnyObject],
                    let data = jsonResponse["data"] as? [[String: AnyObject]],
                    let firstRun = data.first else {
                        throw ApiError.responseMappingFailed
                }
                
                do {
                    let run: Run = try unbox(dictionary: firstRun)
                    return run
                } catch {
                    throw ApiError.responseMappingFailed
                }
        }
    }
    
    static func user(with userId: String) -> Observable<User> {
        return requestJSON(.get, Address.users.url.appendingPathComponent(userId))
            .observeOn(SerialDispatchQueueScheduler(qos: .background))
            .map { _, jsonResponse in
                guard let jsonResponse = jsonResponse as? [String: AnyObject],
                    let data = jsonResponse["data"] as? [String: AnyObject] else {
                        throw ApiError.responseMappingFailed
                }
                
                do {
                    let user: User = try unbox(dictionary: data)
                    return user
                } catch {
                    throw ApiError.responseMappingFailed
                }
        }
    }
}
