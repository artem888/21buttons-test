//
//  AppDelegate.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 27.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import UIKit
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let viewModel = GamesListViewModel(api: SpeedrunAPI.self)
        let controller = GamesListViewController(viewModel: viewModel)
        let rootController = UINavigationController(rootViewController: controller)
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        if self.window != nil {
            self.window!.rootViewController = rootController
            self.window!.makeKeyAndVisible()
        }
        
        return true
    }

}

