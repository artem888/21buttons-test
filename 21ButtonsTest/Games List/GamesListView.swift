//
//  GamesListView.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 28.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import UIKit

protocol GamesListViewDelegate: class {
    func gamesListViewRetryButtonDidTap(_ view: GamesListView)
    func gamesListView(_ view: GamesListView, didSelectGameAt index: Int)
}

final class GamesListView: UIView {
    enum ViewConstants {
        static let cellSpacing: CGFloat = 16.0
        static let cellIdentifier = "cellIdentifier"
    }
    
    weak var delegate: GamesListViewDelegate?
    
    private(set) var collectionView: UICollectionView!
    private var activityIndicator: UIActivityIndicatorView!
    private var retryButton: UIButton!
    
    func render() {
        self.backgroundColor = .white
        self.collectionView = createCollectionView()
        self.activityIndicator = createActivityIndicator()
        self.retryButton = createRetryButton()
    }
    
    func setViewState(_ state: GamesListViewModel.ViewState) {
        switch state {
        case .finished:
            collectionView.isHidden = false
            activityIndicator.stopAnimating()
            retryButton.isHidden = true
        case .error:
            collectionView.isHidden = true
            activityIndicator.stopAnimating()
            retryButton.isHidden = false
        case .loading:
            activityIndicator.startAnimating()
            collectionView.isHidden = true
            retryButton.isHidden = true
        }
    }
    
    @objc func retryTapped() {
        delegate?.gamesListViewRetryButtonDidTap(self)
    }
    
    // MARK: Private
    private func createCollectionView() -> UICollectionView {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0,
                                           left: ViewConstants.cellSpacing,
                                           bottom: 0,
                                           right: ViewConstants.cellSpacing)
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        cv.delegate = self
        cv.contentInset = UIEdgeInsets(top: ViewConstants.cellSpacing,
                                       left: 0,
                                       bottom: 0,
                                       right: 0)
        cv.register(GamesListViewCell.self,
                    forCellWithReuseIdentifier: ViewConstants.cellIdentifier)
        cv.translatesAutoresizingMaskIntoConstraints = false
        addSubview(cv)
        NSLayoutConstraint.activate([
            cv.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            cv.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            cv.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            cv.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor)
        ])
        
        return cv
    }
    
    private func createActivityIndicator() -> UIActivityIndicatorView {
        let a = UIActivityIndicatorView(style: .gray)
        a.translatesAutoresizingMaskIntoConstraints = false
        addSubview(a)
        NSLayoutConstraint.activate([
            a.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            a.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
        
        return a
    }
    
    private func createRetryButton() -> UIButton {
        let btn = UIButton(type: .system)
        btn.setTitle("Retry request", for: .normal)
        btn.addTarget(self,
                      action: #selector(retryTapped),
                      for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        addSubview(btn)
        NSLayoutConstraint.activate([
            btn.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            btn.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
        
        return btn
    }
}

extension GamesListView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sideLength = (collectionView.bounds.width / 2.0) - (ViewConstants.cellSpacing * 1.5)

        return CGSize(width: sideLength,
                      height: sideLength)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return ViewConstants.cellSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        delegate?.gamesListView(self, didSelectGameAt: indexPath.row)
    }
}
