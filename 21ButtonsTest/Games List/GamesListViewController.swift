//
//  GamesListViewController.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 27.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Nuke

final class GamesListViewController: UIViewController {
    private var gamesListView: GamesListView!
    private let viewModel: GamesListViewModelProtocol
    
    private let bag = DisposeBag()
    
    init(viewModel: GamesListViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.gamesListView = GamesListView()
        self.gamesListView.delegate = self
        self.view = self.gamesListView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Speedrun games"
        gamesListView.render()
        rxBind()
        viewModel.refreshGames()
    }
    
    // MARK: Private
    private func rxBind() {
        viewModel.games
            .drive(gamesListView.collectionView.rx
                .items(cellIdentifier: GamesListView.ViewConstants.cellIdentifier)) { index, model, cell in
                    guard let cell = cell as? GamesListViewCell else { return }
                    
                    cell.titleLabel.text = model.name
                    if let url = model.logoUrl {
                        Nuke.loadImage(
                            with: url,
                            options: ImageLoadingOptions(
                                placeholder: UIImage(named: "placeholder"),
                                transition: .fadeIn(duration: 0.33)
                            ),
                            into: cell.logoImageView
                        )
                    }
            }
            .disposed(by: bag)
        
        viewModel.viewState
            .asObservable()
            .subscribe(onNext: { state in
                self.gamesListView.setViewState(state)
            })
            .disposed(by: bag)
    }
}

extension GamesListViewController: GamesListViewDelegate {
    func gamesListView(_ view: GamesListView, didSelectGameAt index: Int) {
        let vm = viewModel.gameDetailsViewModel(at: index)
        let vc = GameDetailsViewController(viewModel: vm)
        self.navigationController?.pushViewController(vc,
                                                      animated: true)
    }
    
    func gamesListViewRetryButtonDidTap(_ view: GamesListView) {
        viewModel.refreshGames()
    }
}
