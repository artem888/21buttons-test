//
//  GamesListViewModel.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 28.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol GamesListViewModelProtocol {
    var api: SpeedrunAPIProtocol.Type { get }
    var games: Driver<[Game]> { get }
    var viewState: Driver<GamesListViewModel.ViewState> { get }
    
    func refreshGames()
    func gameDetailsViewModel(at index: Int) -> GameDetailsViewModel
}

final class GamesListViewModel: GamesListViewModelProtocol {
    enum ViewState {
        case error
        case loading
        case finished
    }
    
    var api: SpeedrunAPIProtocol.Type
    var games: Driver<[Game]>
    var viewState: Driver<GamesListViewModel.ViewState>
    
    private var gamesRelay = BehaviorRelay<[Game]>(value: [])
    private var loadingRelay = BehaviorRelay<Bool>(value: false)
    
    private let bag = DisposeBag()
    
    init(api: SpeedrunAPIProtocol.Type = SpeedrunAPI.self) {
        self.api = api
        self.games = Driver.empty()
        self.viewState = Driver.empty()
        rxBind()
    }
    
    func refreshGames() {
        loadingRelay.accept(true)
        
        api.games().subscribe(onNext: { [weak self] games in
            guard let self = self else { return }
            self.loadingRelay.accept(false)
            self.gamesRelay.accept(games)
        }, onError: { [weak self] _ in
            guard let self = self else { return }
            self.loadingRelay.accept(false)
            self.gamesRelay.accept([])
        }).disposed(by: bag)
    }
    
    func gameDetailsViewModel(at index: Int) -> GameDetailsViewModel {
        let game = gamesRelay.value[index]
        return GameDetailsViewModel(api: SpeedrunAPI.self,
                                    gameName: game.name,
                                    gameLogoURL: game.logoUrl,
                                    gameId: game.id)
    }
    
    // MARK: Private
    private func rxBind() {
        games = gamesRelay.asDriver(onErrorJustReturn: [])
        
        viewState = Observable.combineLatest(loadingRelay, gamesRelay)
            .skip(1)
            .map { (loading, games) -> ViewState in
                if loading && games.isEmpty {
                    return .loading
                } else if games.isEmpty {
                    return .error
                } else {
                    return .finished
                }
            }
            .distinctUntilChanged()
            .asDriver(onErrorJustReturn: .error)
    }
}
