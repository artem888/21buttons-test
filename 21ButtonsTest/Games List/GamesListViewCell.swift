//
//  GamesListViewCell.swift
//  21ButtonsTest
//
//  Created by Artem Sidorenko on 28.03.19.
//  Copyright © 2019 Artem Sidorenko. All rights reserved.
//

import UIKit

final class GamesListViewCell: UICollectionViewCell {
    private enum ViewConstants {
        static let defaultPadding: CGFloat = 16
    }
    
    private(set) var titleLabel: UILabel!
    private(set) var logoImageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        render()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Private
    private func render() {
        self.backgroundColor = .lightGray
        self.titleLabel = createTitleLabel()
        self.logoImageView = createLogoImageView()
    }
    
    private func createTitleLabel() -> UILabel {
        let l = UILabel()
        l.textAlignment = .center
        l.numberOfLines = 2
        l.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(l)
        NSLayoutConstraint.activate([
            l.topAnchor.constraint(equalTo: contentView.topAnchor,
                                   constant: ViewConstants.defaultPadding),
            l.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,
                                       constant: ViewConstants.defaultPadding),
            l.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,
                                        constant: -ViewConstants.defaultPadding)
            ])
        
        return l
    }
    
    private func createLogoImageView() -> UIImageView {
        let iv = UIImageView()
        iv.contentMode = .center
        iv.clipsToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(iv)
        NSLayoutConstraint.activate([
            iv.topAnchor.constraint(equalTo: titleLabel.bottomAnchor,
                                    constant: ViewConstants.defaultPadding),
            iv.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,
                                        constant: ViewConstants.defaultPadding),
            iv.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,
                                         constant: -ViewConstants.defaultPadding),
            iv.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,
                                       constant: -ViewConstants.defaultPadding)
        ])
        
        return iv
    }
}
